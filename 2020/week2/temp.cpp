#include <bits/stdc++.h>
#define ll long long
using namespace std;
int main()
{
    ll t, a, b, c, d;
    cin >> t >> a >> b >> c >> d;
    string s;
    cin >> s;
    ll ans = 0;
    for (auto i : s)
    {
        if (a == c && b == d)
            break;
        if (i == 'N')
        {
            if (d >= 0)
            {
                if (b >= 0)
                {
                    b += (b < d ? 1 : 0);
                }
                else
                {
                    b++;
                }
            }
            else
            {
                if (b < 0)
                {
                    b += (b < d ? 1 : 0);
                }
            }
        }
        else if (i == 'S')
        {
            if (d >= 0)
            {
                if (b >= 0)
                {
                    b -= (b > d ? 1 : 0);
                }
            }
            else
            {
                if (b >= 0)
                {
                    b--;
                }
                else
                {
                    b -= (b > d ? 1 : 0);
                }
            }
        }
        else if (i == 'E')
        {
            if (c >= 0)
            {
                if (a >= 0)
                {
                    a += (a < c ? 1 : 0);
                }
                else
                {
                    a++;
                }
            }
            else
            {
                if (a < 0)
                {
                    a += (a < c ? 1 : 0);
                }
            }
        }
        else if (i == 'W')
        {
            if (c >= 0)
            {
                if (a >= 0)
                {
                    a -= (a > c ? 1 : 0);
                }
            }
            else
            {
                if (a >= 0)
                {
                    a--;
                }
                else
                {
                    a -= (a > c ? 1 : 0);
                }
            }
        }
        ans++;
    }
    if (a == c && b == d)
        cout << ans;
    else
        cout << -1;
}