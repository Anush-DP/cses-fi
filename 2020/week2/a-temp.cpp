#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll lcm(ll a, ll b)
{
    return (a * b) / __gcd(a, b);
}
int main()
{
    ll t, n;
    cin >> t;
    while (t--)
    {
        cin >> n;
        ll l = 1, r = n - 1;
        ll m = INT_MAX, a, b;
        while (l <= r)
        {
            ll tmp = lcm(l, r);
            if (tmp < m)
            {
                m = tmp;
                a = l, b = r;
            }
            l++, r--;
        }
        cerr << a << " " << b << endl;
    }
}