#include <bits/stdc++.h>
#define ll long long
using namespace std;

int main()
{
    ll n;
    cin >> n;
    vector<pair<ll, string>> v(n);
    for (auto &i : v)
    {
        cin >> i.second >> i.first;
        i.first = -i.first;
    }
    sort(v.begin(), v.end());
    for (auto i : v)
        cout << i.second << " " << -i.first << endl;
}