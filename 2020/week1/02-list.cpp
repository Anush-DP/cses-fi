#include <iostream>
using namespace std;
int main()
{
    long long n;
    cin >> n;
    if(n == 3 || n == 2){ cout <<  "NO SOLUTION"; return 0;}
    for (size_t i = 2; i <= n; i+=2)
    {
        cout << i << " ";
    }
    for (size_t i = 1; i <= n; i+=2)
    {
        cout << i << " ";
    }
    return 0;
}