#include <iostream>
#define ll long long
using namespace std;
#define IOS ios_base::sync_with_stdio(0);cin.tie(nullptr);

int main()
{
    IOS
    ll n;
    cin>>n;
    for(int i= 1; i<=n; i++)
        cout << i << " ";
    cout << '\n';
    for(int i= (n&1 ? n-1 : n); i>=2; i-=2)
        cout << i << " ";
    for(int i= 1; i<=n; i+=2)
        cout << i << " ";
    cout << '\n';
        
    return 0;
}
